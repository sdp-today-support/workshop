# Task Concepts

## Ops
- Assembly line

* Work center
  * Machine, Man, Method, Measure

- Constraint (bottleneck)

* Wait time

- WIP

* “Your job as VP of IT Operation …  
  is to ensure the fast flow of planned work …  
  while minimizing the impact of unplanned work…”

## Dev
* The five ideals
  * Locality and Simplicity
  * Focus, Flow and Joy
  * Improvement of Daily Work
  * Psychological Safety
  * Customer Focus

## All
- Small doesn't beat big. Instead, fast beats slow.
   * Developer productivity
   * Build and environment

* Metrics that really matter
  * Employee engagement
  * Customer satisfaction
  * Cash flow

![](./media/phoenix-project/phoenix.jpg) ![](./media/phoenix-project/unicorn.jpg)
