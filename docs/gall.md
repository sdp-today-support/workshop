# 蓋爾定律

一個可行的複雜系統總是從一個可行的簡單系統發展出來。

直接設計的複雜系統並不可行，也無法修修補補讓它可行。

你必須由一個可行的簡單系統重新開始。

[![](./media/gall/gall.png)](https://en.wikipedia.org/wiki/John_Gall_(author)#Gall.27s_law)
