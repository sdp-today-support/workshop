# Cloud Native 的認知

1. [技術架構 - part 2](https://www.youtube.com/watch?v=lud7WHVa0XI) (7:38)
2. [技術架構 - part 1](https://www.youtube.com/watch?v=N3l1WDHLz-M) (11:22)
3. [SDP 方案部署平台](https://www.youtube.com/watch?v=Ea6VF0-yXuw) (6:15) <br>---

4. [Cloud Native Ecosystem 精華版](https://www.youtube.com/watch?v=BvOduog-eOs) (12:57), [原版](https://www.youtube.com/watch?v=Y1qxjU23ACc) (41:23)
5. [CNCF - Who we are 精華版](https://www.youtube.com/watch?v=esl4mxC5bNY) (19:53), [原版](https://www.youtube.com/watch?v=RH62ReFAIXQ) (33:37)
6. [CNCF 官網瀏覽](https://www.youtube.com/watch?v=oHXJ_lWArkk) (26:33)
7. [雲原生核心的精髓](https://www.youtube.com/watch?v=Ml24NE2ENV8) (31:13)
8. [介紹 KRM](https://www.youtube.com/watch?v=oGvSSqe8EPI) - part 1~12 <br>---

9. [Spring Cloud 和 Kubernetes](https://www.youtube.com/watch?v=QwXUjP8SQhY) (21:40)
10. [Platform 的方向感](https://www.youtube.com/watch?v=JVXLB4R74Bw) (19:16)
11. [Strategy Map](https://www.youtube.com/watch?v=TbQGUclyR1o) (23:41)
12. [Infra as Code 和 Config as Data 的差異](https://www.youtube.com/watch?v=I9oNc0kUNis) - part 1~3 <br>---

13. [架構的感覺](https://www.youtube.com/watch?v=PSJiWCv8YEI) (6:32)
14. [組織變動](https://www.youtube.com/watch?v=qCi6xuv3rYE) (19:08)
15. [增加勝率](https://www.youtube.com/watch?v=v0joQ7uMEZM) (23:30)  
    * [Meta](meta.md)   
    * [DORA](https://gitlab.com/sdp-today-support/workshop/-/raw/main/docs/media/youtube/DORA.png)   
    * [IPO](ipo.md) <br>---

16. [K8s 紀錄片](https://www.youtube.com/watch?v=BE77h7dmoQU) - part 1~2
