# [原則](https://www.youtube.com/watch?v=lud7WHVa0XI)

* 用版控系統溝通源碼和設定檔
* 用自動化工具做簡易的工作
* 分離 CI 和 CD
* 運維提供 alpha 環境給技術來開發系統
* 用 K8s 管理容器

[![](./media/principle/flow.png)](https://hackmd.io/sWLfCDjEQpO6KtWMrI8zRg)
