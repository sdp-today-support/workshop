# Kubernetes API
* API server
  * [Official doc](https://kubernetes.io/docs/concepts/overview/kubernetes-api/)
  * [Controlling Access](https://kubernetes.io/docs/concepts/security/controlling-access/)
  * [kubeconfig](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/)
* Code
  * [Go](https://go.dev/)
  * [kubebuilder](https://github.com/kubernetes-sigs/kubebuilder)
  * [Go kit](https://gokit.io/)
* API
  * [OpenAPI](https://www.openapis.org/) - [map](https://openapi-map.apihandyman.io/)
  * [Swagger](https://swagger.io/tools/open-source/getting-started/)
  * [The Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
* Tutorials
  * [REST Servers in Go](https://eli.thegreenplace.net/2021/rest-servers-in-go-part-4-using-openapi-and-swagger/)
  * [REST API + OpenAPI docs](https://dev.to/vearutop/tutorial-developing-a-restful-api-with-go-json-schema-validation-and-openapi-docs-2490)
