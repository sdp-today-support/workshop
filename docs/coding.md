# Coding

## 2023-04-21

這個社團成立了 21 個月，描述雲原生技術先從運維團隊扎根，然後影響技術團隊的過程。由於產業的異質性，常聯想到動漫頭文字D的膠帶殊死戰，因為右手綁在方向盤上進行下坡賽，讓拓海在危險中找到感覺和訣竅，也因而改善了甩尾角度過大的毛病。最近實做出理想的雲原生應用，就是在呈現用運維團隊的角度改善後的技術選擇，避開了許多技術團隊的盲點。由於組織變動，目前的團隊將加入一個全新的公司，走向更大的格局。在此感謝大家對這個社團的關注，若下一個旅程有戲，很可能會開個新的社團來分享。 [link](https://www.facebook.com/groups/514614153094343)

## 2023-03-31

從 2022 年 11 月中起，發生一連串的巧合，讓我在技術上整合並實作出理想中的雲原生應用，在此做個紀錄。

1. “雲原生核心的精髓”為根源，用來判斷並選用技術大幅增加了成功機率。
2. 在 workshop 公開技術的方向感，若沒做這件事，連自己的想法都不夠清晰，更何況要推動集團的走向。
3. 用 backstage 開源專案打造開發平台，讓慢慢長出來的應用系統有存活的地方。
4. 用 tekton 實作自動化流程，清楚的邏輯層次吊打複雜難懂的黑箱工具。
5. 選用雲原生慨念好並且接地氣的 srs 開源專案，開發大流量的應用。
6. 用 swaggest 實作 The Clean Architecture，做出自帶 OAS3 的 API server。
7. 用 React 18 + Next.js 13，進入新一代的前端世界。

Links:

1. [雲原生核心的精髓](https://www.youtube.com/watch?v=Ml24NE2ENV8)
2. [workshop](https://gitlab.com/sdp-today-support/workshop)
3. [backstage](https://backstage.io/)
4. [tekton](https://tekton.dev/)
5. [srs](https://github.com/ossrs)
6. [swaggest](https://github.com/swaggest/rest)
7. [Next.js](https://beta.nextjs.org/docs/getting-started)
