# Admin

## 課程
* Cloud Native 的認知
* K8s 創建研究
* 深入 K8s RBAC
* 用 kubie 操作多個 K8s clusters
* 深入 kubectl 和 kustomize
* 分離 CI 和 CD 的 repo 架構
* Automation 創建研究
* Sub-systems
* 用 SDP 呈現系統

## 教材

### GCP
* IAM
* GKE
* Artifact registry
* Cloud storage

### RBAC
[Predefined GKE Roles](https://cloud.google.com/kubernetes-engine/docs/how-to/iam#predefined)

[Using RBAC Authorization](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)

```
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: alpha-admin
subjects:
- kind: Group
  name: alpha-admin@example.com
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
```

```
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: alpha-dev
  namespace: lab
subjects:
- kind: Group
  name: alpha-dev@example.com
roleRef:
  kind: ClusterRole
  name: edit
  apiGroup: rbac.authorization.k8s.io
```

### Gitlab
* Runner [video](https://drive.google.com/file/d/1ogLEQrIW_INVqFKA8JkfuXSGJJEnEkhT/view?usp=sharing) (21:20)
  * [Gitlab CICD](https://hackmd.io/dCSJtmu5T_CDJOOpzdecSQ?view)
  * [GitLab Runner | GitLab](https://docs.gitlab.com/runner/)
  * [Install GitLab Runner](https://docs.gitlab.com/runner/install/index.html)
  * [Best practices | GitLab](https://docs.gitlab.com/runner/best_practice/index.html)
  * [Run your CI/CD jobs in Docker containers | GitLab](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)
  * [Advanced configuration | GitLab](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)
  * [GitLab Runner monitoring | GitLab](https://docs.gitlab.com/runner/monitoring/index.html)
  * [Troubleshoot GitLab Runner | GitLab](https://docs.gitlab.com/runner/faq/index.html)
  * [The Kubernetes executor | GitLab](https://docs.gitlab.com/runner/executors/kubernetes.html)
  * [Configuring runners | GitLab](https://docs.gitlab.com/ee/ci/runners/configure_runners.html)
  * [GitLab Kubernetes Agent | GitLab](https://docs.gitlab.com/ee/user/clusters/agent/index.html)
  * [k8s CI prototyping](https://co-clro.bitrix24.cn/workgroups/group/24/tasks/task/view/1841/?MID=10325)
  * [GitLab Runner Helm Chart | GitLab](https://docs.gitlab.com/runner/install/kubernetes.html)
* CI [video](https://drive.google.com/file/d/1b7ig5iIKo62Wf3Y7xg0kKhztFMyPsAES/view?usp=sharing) (16:24)
  * [GitLab CI/CD | GitLab](https://docs.gitlab.com/ee/ci/)
  * [CI/CD pipelines | GitLab](https://docs.gitlab.com/ee/ci/pipelines/#types-of-pipelines)
  * [Pipelines for merge requests | GitLab](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html)
  * [Pipelines for merged results | GitLab](https://docs.gitlab.com/ee/ci/pipelines/pipelines_for_merged_results.html)
  * [Use Docker to build Docker images | GitLab](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
  * [Keyword reference for the `.gitlab-ci.yml` file | GitLab](https://docs.gitlab.com/ee/ci/yaml/index.html)
  * [The `.gitlab-ci.yml` file | GitLab](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html#)
  * [Predefined variables reference | GitLab](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#)
  * [GitLab CI/CD variables | GitLab](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-types)
  * [Jobs | GitLab](https://docs.gitlab.com/ee/ci/jobs/#expand-and-collapse-job-log-sections)
  * [Environments and deployments | GitLab](https://docs.gitlab.com/ee/ci/environments/#configure-kubernetes-deployments)

### 安裝 Runner
[GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)

* Check available GitLab Runner Helm Chart versions
  ```
  $ helm search repo -l gitlab/gitlab-runner
  NAME                    CHART VERSION   APP VERSION     DESCRIPTION  
  gitlab/gitlab-runner    0.31.0          14.1.0          GitLab Runner
  gitlab/gitlab-runner    0.30.0          14.0.0          GitLab Runner
  gitlab/gitlab-runner    0.29.0          13.12.0         GitLab Runner
  gitlab/gitlab-runner    0.28.0          13.11.0         GitLab Runner
  ```

* Pick gitlab group to install runner
  * Get the values of `gitlabUrl` and `runnerRegistrationToken` from group's `Settings-> CI/CD -> Runners`.
  * Put the values in `values.yaml`.
  * [Default runner config file](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml)
  ```
  % cat values.yaml

  gitlabUrl: https://gitlab.com/
  runnerRegistrationToken: J11122233344455566y
  replicas: 1
  rbac:
    create: true
  runners:
    privileged: true
    locked: false
    tags: workshop-ci
  ```

* Access cluster
  ```
  [runner|default] % k get ns
  NAME                     STATUS   AGE
  default                  Active   78d
  kube-node-lease          Active   78d
  kube-public              Active   78d
  kube-system              Active   78d
  ```

* Create namespace
  ```
  [runner|default] % k create ns runner
  namespace/runner created

  [runner|default] % k get ns runner
  NAME        STATUS   AGE
  runner   Active   37s

  [runner|default] % kubie ns

  [runner|runner] %
  ```

* Install with Helm

  [Official doc](https://docs.gitlab.com/runner/install/kubernetes.html#installing-gitlab-runner-using-the-helm-chart)

  ```
  [runner|runner] % helm install --namespace runner workshop-ci -f values.yaml gitlab/gitlab-runner   
  NAME: workshop-ci
  LAST DEPLOYED: Sat Oct 16 12:46:52 2021
  NAMESPACE: runner
  STATUS: deployed
  REVISION: 1
  TEST SUITE: None
  NOTES:
  Your GitLab Runner should now be registered against the GitLab instance reachable at: "https://gitlab.com/"

  Runner namespace "runner" was found in runners.config template.

  [runner|runner] % helm ls
  NAME            NAMESPACE       REVISION        UPDATED                                 STATUS         CHART                   APP VERSION
  workshop-ci        runner       1               2021-10-16 12:46:52.986287 +0800 CST    deployed       gitlab-runner-0.31.0    14.1.0     
  ```

* Check runner is running in the correct gitlab group and project

  Use gitlab WebUI.

### 分離 CI 和 CD
* list.yaml
  ```
  clock:
    image: "asia-east1-docker.pkg.dev/example-321303/workshop/clock:v0.1.4-jane"
    yaml: "gs://lab/clock-v0.1.4-jane.tar.gz"
    repo: "https://gitlab.com/shadow-tranquil/workshop/clock"
    tag: "v0.1.4-jane"
  ```

* .gitlab-ci.yml
  * initial
  ```
  default:
    tags:
      - workshop-ci

  workflow:
    rules:
      - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
        when: never
      - if: $CI_COMMIT_BRANCH
        when: never
      - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_SLUG =~ /^v?\d+.\d+.\d+-alpha$/'      

  stages:
    - build_docker
    - modify_yaml

  build_docker:
    stage: build_docker 
    image: alpine:3.13
    script:    
      - |
        echo "First Stage"      

  modify_yaml:
    stage: modify_yaml  
    image: alpine:3.13  
    dependencies:
      - build_docker
    script:    
      - |
        echo "Second Stage"     
  ```

  * final  
    * 這個範例有幾個地方不會被技術和運維團隊在真實情境中採用
    * MAC 指定產出舊的 RSA 要加參數 -m PEM，`ssh-keygen -m PEM -t rsa -b 4096`。或者別再用 rsa。

    ```
    variables:
      DOCKER_DRIVER: overlay2
      DOCKER_TLS_CERTDIR: ""
      DOCKER_HOST: tcp://docker:2375
      GAR_PATH: asia-east1-docker.pkg.dev/workshop-321303/lab/clock
      GCS_PATH: gs://lab-k8s
      INTEGRATION_REPO: git@gitlab.com:workshop/training/integration.git
    default:
      image: docker:19.03.13-dind
      services:
        - docker:19.03.13-dind
      tags:
        - workshop-ci

    .gcp_tasks: &gcp_tasks |-
        apk update
        apk add bash git curl wget py3-pip python3
        curl -sSL https://sdk.cloud.google.com | bash
        export PATH="/root/google-cloud-sdk/bin:$PATH"
        echo $GCP_IAM > /gcp-gsa.json
        gcloud auth activate-service-account --key-file /gcp-gsa.json
        gcloud auth configure-docker asia-east1-docker.pkg.dev

    .git_setup: &git_setup |-
        apk update
        apk add git bash openssh
        wget https://github.com/mikefarah/yq/releases/download/v4.21.1/yq_linux_386.tar.gz
        tar zxvf yq_linux_386.tar.gz
        mv yq_linux_386 /usr/local/bin/yq
        yq --version
        mkdir -p ~/.ssh/
        cp $LAB_USER ~/.ssh/id_rsa
        chmod 400 ~/.ssh/id_rsa
        ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
        git config --global user.email "jane@example.com"
        git config --global user.name "Jane Doe"

    workflow:
      rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
          when: never
        - if: $CI_COMMIT_BRANCH
          when: never
        - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_SLUG =~ /^v?\d+.\d+.\d+-alpha$/'
          variables:
            INTEGRATION: "alpha"
                        
    stages:
      - build_docker
      - modify_yaml

    build_docker:
      stage: build_docker  
      script:    
        - *gcp_tasks
        - |
          echo "First Stage"
          apk add go
          CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o clock
          docker build -t $GAR_PATH:$CI_COMMIT_TAG --build-arg BRANCH=$INTEGRATION . 
          docker push $GAR_PATH:$CI_COMMIT_TAG

    modify_yaml:
      stage: modify_yaml  
      image: alpine:3.13  
      dependencies:
        - build_docker
      script:    
        - *gcp_tasks
        - *git_setup
        - |
          echo "Second Stage"
          echo $CI_PROJECT_PATH      
          git config --global pull.rebase false
          git pull origin $INTEGRATION    
          cd k8s-yaml
          yq e -i '.spec.template.spec.containers[0].image="'${GAR_PATH}:${CI_COMMIT_TAG}'"' deployment.yaml
          git branch  $INTEGRATION
          git checkout  $INTEGRATION
          git status
          ls
          git add .
          git commit -m "modified: k8s-yaml/deployment.yaml"
          git remote rm origin && git remote add origin git@gitlab.com:$CI_PROJECT_PATH.git
          git push origin $INTEGRATION
          git tag -d $CI_COMMIT_TAG
          git tag $CI_COMMIT_TAG
          git push -o ci.skip origin :refs/tags/$CI_COMMIT_TAG
          git push -o ci.skip origin refs/tags/$CI_COMMIT_TAG
          cd ..
          tar zcvf $CI_PROJECT_NAME-$CI_COMMIT_TAG.tar.gz k8s-yaml
          gsutil cp $CI_PROJECT_NAME-$CI_COMMIT_TAG.tar.gz $GCS_PATH/$CI_PROJECT_NAME/$CI_PROJECT_NAME-$CI_COMMIT_TAG.tar.gz
          git clone $INTEGRATION_REPO
          cd integration
          git checkout main
          yq e -i '."'${CI_PROJECT_NAME}'".image="'${GAR_PATH}:${CI_COMMIT_TAG}'"' $INTEGRATION.yaml
          yq e -i '."'${CI_PROJECT_NAME}'".yaml="'${GCS_PATH}/${CI_PROJECT_NAME}/${CI_PROJECT_NAME}-${CI_COMMIT_TAG}.tar.gz'"' $INTEGRATION.yaml
          yq e -i '."'${CI_PROJECT_NAME}'".repo="'${CI_PROJECT_URL}'"' $INTEGRATION.yaml
          yq e -i '."'${CI_PROJECT_NAME}'".tag="'${CI_COMMIT_TAG}'"' $INTEGRATION.yaml
          git add .
          git commit -m "$CI_PROJECT_NAME:$CI_COMMIT_TAG"
          git remote rm origin && git remote add origin $INTEGRATION_REPO
          git push origin main
    ```

### Ingress
```
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: letsencrypt
  namespace: alpha
spec:
  acme:
    email: jane.doe@example.io
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      # Secret resource that will be used to store the account's private key.
      name: issuer-account-key
    solvers:
    - http01:
        ingress:
          name: sdp
```

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: sdp
  namespace: alpha
  annotations:
    kubernetes.io/ingress.allow-http: "false"
    cert-manager.io/issuer: letsencrypt
    acme.cert-manager.io/http01-edit-in-place: "true"
spec:
  tls:
  - hosts:
    - alpha.sdp.today
    secretName: tls-cert
  defaultBackend:
    service:
      name: sdp
      port:
        name: http
```
