# Tools

* git, curl, tar
* [gcloud](https://cloud.google.com/sdk/docs/install), gsutil
* [docker](https://docs.docker.com/get-docker/)
* [kubectl](https://kubernetes.io/docs/tasks/tools/)
* [kustomize](https://kubectl.docs.kubernetes.io/installation/kustomize/) <br>---

* [kubie](https://github.com/sbstp/kubie) + [fzf](https://github.com/junegunn/fzf) + [tmux](https://github.com/tmux/tmux/wiki/Installing)
* [go](https://golang.org/)
* [特殊工具](special-tools.md)

## .zshrc
```
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/myname/code/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/myname/code/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/myname/code/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/myname/code/google-cloud-sdk/completion.zsh.inc'; fi

source <(kubectl completion zsh)
alias k=kubectl
complete -F __start_kubectl k

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# for kubie
alias kic="/opt/homebrew/bin/kubie ctx"
alias kin="/opt/homebrew/bin/kubie ns"
```

## kubie.yaml
```
shell: zsh

# Configure where to look for kubernetes config files.
configs:

    # Include these globs.
    # Default: values listed below.
    include:
        - ~/.kube/config
        - ~/.kube/*.yml
        - ~/.kube/*.yaml
        - ~/.kube/configs/*.yml
        - ~/.kube/configs/*.yaml
        - ~/.kube/kubie/*.yml
        - ~/.kube/kubie/*.yaml

    # Exclude these globs.
    # Default: values listed below.
    # Note: kubie's own config file is always excluded.
    exclude:
        - ~/.kube/kubie.yaml

# Prompt settings.
prompt:
    # Disable kubie's custom prompt inside of a kubie shell. This is useful
    # when you already have a prompt displaying kubernetes information.
    # Default: false
    disable: false

    # When using recursive contexts, show depth when larger than 1.
    # Default: true
    show_depth: true

    # When using zsh, show context and namespace on the right-hand side using RPS1.
    # Default: false
    zsh_use_rps1: false

    # When using fish, show context and namespace on the right-hand side.
    # Default: false
    fish_use_rprompt: false

# Behavior
behavior:
    # Make sure the namespace exists with `kubectl get namespaces` when switching
    # namespaces. If you do not have the right to list namespaces, disable this.
    # Default: true
    validate_namespaces: true

    # Enable or disable the printing of the 'CONTEXT => ...' headers when running
    # `kubie exec`.
    # Valid values:
    #   auto:   Prints context headers only if stdout is a TTY. Piping/redirecting
    #           kubie output will auto-disable context headers.
    #   always: Always prints context headers, even if stdout is not a TTY.
    #   never:  Never prints context headers.
    # Default: auto
    print_context_in_exec: auto
```

## vim
```
gg , G

0 $ f F t T

w , W , b , B

/pattern , ?pattern

x , X , dw , dW , d/pattern , c/pattern

dt<char> , ct<char>

v , V
y, d, p

H, M, L
zz zt zb
^u ^d

u , ^r
```

## tmux
```
# session management
tmux ls (or tmux list-sessions)
tmux new -s session-name
tmux attach -t [session name]
tmux kill-session -t session-name

Ctrl-b d Detach from session
Ctrl-b s list out sessions

Ctrl-b c Create new window
Ctrl-b d Detach current client
Ctrl-b n Move to the next window
Ctrl-b p Move to the previous window
Ctrl-b & Kill the current window
Ctrl-b , Rename the current window
Ctrl-b q Show pane numbers (used to switch between panes)
Ctrl-b o Switch to the next pane
Ctrl-b ? List all keybindings

# moving between windows
Ctrl-b n (Move to the next window)
Ctrl-b p (Move to the previous window)
Ctrl-b w (List all windows / window numbers)
Ctrl-b window number (Move to the specified window number, the
default bindings are from 0 -- 9)

# Tiling commands
Ctrl-b % (Split the window vertically)
Ctrl-b " (Split window horizontally)
Ctrl-b o (Goto next pane)
Ctrl-b q (Show pane numbers, when the numbers show up type the key to go to that pane)
Ctrl-b { (Move the current pane left)
Ctrl-b } (Move the current pane right)

# Make a pane its own window
Ctrl-b : "break-pane"
Ctrl-b : "swap-window -s 0 -t 1"
Ctrl-b : "resize-pane -U 5"

# add to ~/.tmux.conf
bind | split-window -h
bind - split-window -v

# vi keys in copy mode
Ctrl-b [ - enter copy mode
Ctrl-b PageUp - enter copy mode
space - start selection
enter - end selection
Ctrl-b ] - paste
```
