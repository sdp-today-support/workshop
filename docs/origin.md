# 根源

## 文以載道
互動與表達，可考慮含有以下內涵

## 人皆聖賢
中華文化幾千年的覺悟，就是接納

接納人人都做得到

## 允執厥中
源頭的一點差異，會產生極大的影響

處事中庸，避免偏激，降低影響幅度，穩定局勢

不在源頭的變化，影響力小

心是源頭，邏輯思維不是

## 廢心用形

(這裡的「心」是指邏輯思維，跟前一段的用法不同)

邏輯思維循環不停，要覺察當下的感受

> Mindfulness is awareness that arises through paying attention,  
> on purpose,  
> in the present moment,  
> non-judgementally.  
>  
> It’s about knowing what is on your mind.  

> 接納、無常、放下  
> 用心若鏡

* 原始情感
  * 尋求 SEEKING
  * 憤怒 ANGER
  * 恐懼 FEAR
  * 慾望 LUST
  * 關懷 CARE
  * 悲傷 SADNESS、驚慌 PANIC
  * 遊戲 PLAY

## 一以貫之
從根源看事物，若不尋找根源，容易迷失在表象

## 二入四行
- 無有分別，寂然無為
- 逢苦不憂
- 得失從緣，心無增減
- 有求皆苦，無求即樂
- 眾相斯空，無染無著，無此無彼