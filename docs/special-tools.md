# 特殊工具

## Network
* [mtr](https://github.com/traviscross/mtr) combines the functionality of the 'traceroute' and 'ping' programs in a single network diagnostic tool.
  * [What is MTR?](https://www.pcwdld.com/what-is-mtr-and-howto-troubleshoot-connections)

    ```
    %sudo mtr -r -n -i 0.2 -c 10 -o "L BAWV MI" 47.117.69.86
    Start: 2023-01-14T17:31:07+0800
    HOST: mbp                         Loss%   Best   Avg  Wrst StDev  Javg Jint
      1.|-- 192.168.1.1               20.0%    3.5   8.1  22.2   7.4   3.7 22.2
      2.|-- 168.95.98.254              0.0%    4.7   8.2  12.6   2.8   3.7 28.9
      3.|-- 168.95.84.102              0.0%    5.9   9.2  17.1   3.2   3.5 26.9
      4.|-- 220.128.3.134              0.0%    5.1   7.9  13.8   2.6   2.1 15.6
      5.|-- 220.128.24.181             0.0%    7.8  10.3  14.3   1.9   1.4 11.2
      6.|-- 220.128.11.153             0.0%    4.6   6.1   9.2   1.5   1.6 12.3
      7.|-- 220.128.10.193             0.0%    4.8   8.9  16.5   3.5   2.8 23.1
      8.|-- 183.91.34.121              0.0%   51.9  60.5  66.8   5.1   4.6 38.4
      9.|-- 202.97.6.5                50.0%   67.2  77.5  85.6   7.4   8.5 38.2
     10.|-- 202.97.12.189              0.0%   94.6 113.3 145.1  14.6  14.2 107.6
     11.|-- 202.97.57.26              70.0%   86.3  91.2  95.8   4.8   3.2  9.2
     12.|-- ???                       100.0    0.0   0.0   0.0   0.0   0.0  0.0
     13.|-- ???                       100.0    0.0   0.0   0.0   0.0   0.0  0.0
     14.|-- 180.163.53.74              0.0%  146.1 160.7 174.1   9.1   7.9 64.1
     15.|-- ???                       100.0    0.0   0.0   0.0   0.0   0.0  0.0
     16.|-- 116.251.107.10            60.0%   74.0  89.2 103.6  12.3  11.5 43.5
     17.|-- ???                       100.0    0.0   0.0   0.0   0.0   0.0  0.0
     18.|-- ???                       100.0    0.0   0.0   0.0   0.0   0.0  0.0
     19.|-- 47.117.69.86              10.0%  146.9 159.0 176.1  10.2   8.1 56.4
    ```

* [netshoot](https://github.com/nicolaka/netshoot): a Docker + Kubernetes network trouble-shooting swiss-army container
* [用 Wireshark 分析 TCP 吞吐瓶颈](https://www.kawabangga.com/posts/4794)

## 前端性能優化
* [浏览器performance工具介绍及内存问题表现与监控内存的几种方式](http://t.zoukankan.com/websiteblogs-p-14295619.html)
* [前端性能优化——Performance的使用攻略](https://www.cnblogs.com/mushanya/p/16827522.html)
