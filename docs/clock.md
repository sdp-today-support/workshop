# Clock

## A simple golang http server
* Goal - 手動把模組從源代碼部署到 prod 環境
* Demo
  * 身份和權限 - infra, K8s
  * 版本管控 - branch, tag
  * 源代碼 - go, http, logs, metrics
  * 分離 CI 和 CD - 開發和部署流程
  * 宣告式自動化
* Repo
  * [https://gitlab.com/training568/clock](https://gitlab.com/training568/clock)
