# 每日事項

* [DORA 星狀圖](https://gitlab.com/sdp-today-support/workshop/-/raw/main/docs/media/youtube/DORA.png)
  * 活動

- [Meta](https://gitlab.com/sdp-today-support/workshop/-/blob/main/docs/meta.md)
  - 合作

* [孫子兵法](https://gitlab.com/sdp-today-support/workshop/-/blob/main/docs/suntzu.md)
  * 生存

- [根源](https://gitlab.com/sdp-today-support/workshop/-/blob/main/docs/origin.md)
  - 接納
