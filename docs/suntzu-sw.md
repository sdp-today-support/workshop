# 兵法聯想

[孫子兵法](./suntzu.md)

## 始計第一
* 確認目標 (Clarify goal)

## 作戰第二
* 分析資源 (Analyze resources)

## 謀攻第三
* 比較方法 (Compare methods)

## 軍形第四
* Situation

## 兵勢第五
* Options

## 虛實第六
* Focus

## 軍爭第七
* Engagement

## 九變第八
* Autonomy

## 行軍第九、地形第十、九地第十一  
* Development

## 火攻第十二
* Tools

## 用間第十三
* Open Source Software
