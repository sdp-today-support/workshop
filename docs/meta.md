# Meta

* [Change computing forever](https://oxide.computer/principles) - [Bryan Cantrill](https://en.wikipedia.org/wiki/Bryan_Cantrill)  
  Computing is our shared passion; our calling is to advance the state of the art,  
  bringing those advances to the broadest possible audience.

  - Principles
    - Integrity, honesty, decency

* [Culture](https://www.stacklok.com/post/stacklok-culture) - [Craig McLuckie](https://www.linkedin.com/in/craigmcluckie/)

* [Mystery of Scaling](https://hackmd.io/IVWffHI_TR61SlIlyJ-9xg) - [Ben Horowitz](https://en.wikipedia.org/wiki/Ben_Horowitz)
  * Challenge
    * Communication
    * Common knowledge
    * Decision making
  * Solution
    * Specialization
    * Organizational design
    * Process

* [Accountability vs. Creativity](https://hackmd.io/TKrvByzhQai-U5wQqVArqg) - [Ben Horowitz](https://en.wikipedia.org/wiki/Ben_Horowitz)
  * Accountability for effort
  * Accountability for promises
  * Accountability for results
    * How senior is employee?
    * How hard was it?
    * Was the original risk the right one to take?

* [Metrics that really matter](https://www.linkedin.com/pulse/three-ways-take-your-companys-pulse-jack-welch/) - [Jack Welch](https://en.wikipedia.org/wiki/Jack_Welch)
  * Employee engagement
  * Customer satisfaction
  * Cash flow

- 員工投入度
  
  ![](./media/meta/UQXBQbQ.png)

* [Motivation](https://www.danpink.com/books/drive/)
  * Autonomy
  * Mastery
  * Purpose
    * Advance the state of the art

* [Task Concepts](phoenix-project.md)

* [兵法聯想](./suntzu-sw.md)

* [根源](./origin.md)
