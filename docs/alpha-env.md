# Alpha 環境

## 環境基礎
* 雲服務權限設定 (GKE, GAR, GCS)
* GitLab repo 結構設計 (分離 CI 和 CD)
* 自動化流程 (.gitlab-ci.yml, tekton)
* 第三方服務 (網域, 高防, 統計, 推送, 數據, 影音)

## K8s sub-systems
* gateway
  * cert-manager
  * ingress-controller
* logs
  * fluent-bit
  * elasticsearch-operator
* monitoring
  * grafana
  * prometheus-operator

## URL
* gateway
  * www.&#8204;a.com
  * admin.a.com
  * m.a.com
  * h5.a.com
  * pc.a.com
  * app.a.com
* logs
  * kibana.a.com
* monitoring
  * grafana.a.com
