# Spring Cloud 和 Kubernetes

* K8s 是一個「容器管理系統」。

* K8s 抽離了「分散式系統」的必要功能，成為建立「高可用」系統的主流。

* [YouTube](https://www.youtube.com/watch?v=QwXUjP8SQhY) (21:40)

![](./media/spring-k8s/spring-k8s.png)
