# workshop

舊版：[Workshop (Admin)](https://docs.google.com/presentation/d/12A_fcgCEdP6jEHC38GAbjS0cELMYXzciInjLu8me_r4/edit?fbclid=IwAR3NKfIS_vUnMWkEajb_lnBv9QhNRkELfEySHpiGD257BTqXLQYfXr3R0nI#slide=id.g11837237a3d_0_25)

* 看 mkdocs 可以用 npx，這是含在 Node.js 裡面的工具。  
  * npx is a tool to run Node executables straight from the registry.  
    This tool is part of your Node.js installation.  
  * [安裝 Node.js](https://backstage.io/docs/getting-started/#prerequisites)  
  * 在 local 的 repo 執行 npx 指令來看文件。  
    ```
    npx @techdocs/cli serve:mkdocs --no-docker
    ```
